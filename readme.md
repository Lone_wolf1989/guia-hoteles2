# projecto guia-hoteles

hola amigos este es el proyecto de guia hoteles, mediante el  cual desarrollo mis habilidades de desarrollo web con el framework bootstrap 4.5\

para esta entrega de proyecto, usando la documentacion oficial de bootstrap con los temas vistos en los videos del mismo y algunas librerias adicionales (no vistas) he logrado

* [x] utilizar el sistema de grillas de grid (col, sm,md, lg, y xl)
* [x] incorporar una cantidad ilimitada de elementos utilizando el sistema/ 
      flexbox de bootstrap
* [x] incorporar iconos oficiales de sitios web (facebook, twitter, instagram)/
      a travez de la libreria de fontawesome
* [x] usar la libreria de bootstrap la cual me permite utilizar mensajes en 
      ventanas emergentes de una manera vistosa y bonita

* [x] un menu principal en el cual los usuarios podran acceder a mas opciones en la plataforma
      (pero no podran acceder a ellas debido a que el sitio esta en construccion por el momento)

* [x] le doy identidad al proyecto dandole un icono oficial y no el que trae por defecto
        la ventana del browser(navegador)


